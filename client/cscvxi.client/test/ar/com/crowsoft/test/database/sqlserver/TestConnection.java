package ar.com.crowsoft.test.database.sqlserver;

import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Scanner;

import org.apache.log4j.Logger;

public class TestConnection {
	
	private static final Logger logger = Logger.getRootLogger();

	public static void main(String[] args) {
		if (args.length < 4) {
			printUsage();
		}
		else if(args[0].equalsIgnoreCase("help"))
			printUsage();
		else if (!args[0].equals("d"))
			printUsage();
		else if (!args[2].equals("c"))
			printUsage();
		else {
			String driverClass = args[1];
			String connectionString = args[3];
			try {
				Class.forName(driverClass);
		        Connection con = DriverManager.getConnection(connectionString);
		        logger.info("Connected");	
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
    }
	
	private static void printUsage() {
		System.out.println("Usage: TestConnection d {driver_class} c {connection_string}");
        System.out.println("Presss enter to continue");
        Scanner sc = new Scanner(System.in);
        sc.nextLine();
	}
}
