package ar.com.crowsoft.cscvxi.client;

import ar.com.crowsoft.cscvxi.CSCVXI;
import ar.com.crowsoft.cscvxi.business.logic.ConfigurationBL;
import ar.com.crowsoft.cscvxi.business.object.Configuration;
import ar.com.crowsoft.http.CSHttpRequest;

public class CSClient {

	private Configuration configuration = null;
    private static CSClient instance;
    
    public static void initialize() throws Exception {
    	instance = new CSClient();
        CSHttpRequest.Initialise(instance.getConfig().getShowDebug());
        CSNotifications.Initialise(instance.getConfig().getClientKey());
    }
    
    protected CSClient() throws Exception {
    	ConfigurationBL configBL = (ConfigurationBL) CSCVXI.getBean("ConfigurationBL");
    	this.configuration = configBL.getConfiguration();  
    }
    
    private Configuration getConfig() {
    	return configuration;
    }
    
    public static String getLogin() {
    	return instance.getConfig().getMeliUser();
    }
    
    public static String getPassword() {
    	return instance.getConfig().getPassword();
    }
    
    public static CSClient getInstance() {
    	return instance;
    }
    
    public static Configuration getConfiguration() {
    	return instance.getConfig();
    }

}
