package ar.com.crowsoft.cscvxi.client;

import java.util.ArrayList;
import java.util.LinkedHashMap;

import ar.com.crowsoft.cscvxi.CSCVXI;

public class CSResponse<T> {

    private Boolean mSuccess;
    private int mErrorCode = CSCVXI.ERROR;
    private ArrayList<T> mData = null;
    private LinkedHashMap<String, T> mMap = null;
    private int mNumResults = 0;
    private String mErrorMessage = "";
    private T mObject;
    
    public Boolean getSuccess() {
        return mSuccess;
    }

    public int getErrorCode() {
        return mErrorCode;
    }

    public String getErrorMessage() {
        return mErrorMessage;
    }
    
    public ArrayList<T> getData() {
        return mData;
    }

    public LinkedHashMap<String, T> getMap() {
        return mMap;
    }

    public int getNumResults() {
        return mNumResults;
    }
    
    public T getObject() {
        return mObject;
    }
    
    public void setObject(T object) {
        mObject = object;
    }
    
    public CSResponse(Boolean success, int errorCode) {
        mSuccess = success;
        mErrorCode = errorCode;
    }
    
    public CSResponse(Boolean success, int errorCode, ArrayList<T> data, int numResults) {
        mSuccess = success;
        mErrorCode = errorCode;
        mData = data;
        mNumResults = numResults;
    }

    public CSResponse(Boolean success, int errorCode, LinkedHashMap<String, T> map) {
        mSuccess = success;
        mErrorCode = errorCode;
        mMap = map;
    }
    
    public CSResponse(Boolean success, int errorCode, T object) {
        mSuccess = success;
        mErrorCode = errorCode;
        mObject = object;
    }

    public CSResponse(int errorCode, String errorMessage) {
        mSuccess = false;
        mErrorCode = errorCode;
        mErrorMessage = errorMessage;
    }
}
