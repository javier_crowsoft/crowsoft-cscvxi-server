package ar.com.crowsoft.cscvxi.client;

import java.util.ArrayList;
import java.util.LinkedHashMap;

import ar.com.crowsoft.cscvxi.CSCVXI;
import ar.com.crowsoft.http.CSEncrypt;
import ar.com.crowsoft.http.CSHttpRequest;
import ar.com.crowsoft.http.CSHttpRequestListener;
import ar.com.crowsoft.http.CSHttpResponse;
import ar.com.crowsoft.http.CSHttpRequestUrl;

import org.json.JSONArray;
import org.json.JSONObject;

public class CSNotifications {
    
    private CSRequestListener<JSONArray> mRequestListener;

    public void setRequestListener(CSRequestListener<JSONArray> requestListener) {
        mRequestListener = requestListener;
    }
    
    private static String SECTION;
    private static String GET;
    private static String UPDATE;
    
    static void Initialise(String clientKey) throws Exception {
        SECTION = CSEncrypt.md5("notifications-" + clientKey);
        GET = CSEncrypt.md5("notifications-get-" + clientKey);
        UPDATE = CSEncrypt.md5("notifications-update-" + clientKey);
    }
    
    public void get(String what) {
        CSHttpRequest request = new CSHttpRequest();
        request.setHttpRequestListener(new CSHttpRequestListener() {
            public void onRequestFinished(CSHttpResponse response) {
                if (response.getSuccess()) {
                    requestFinished(response.getData());
                }            
                else {
                    requestFailed();
                }
            }
        });
        try {
	        	LinkedHashMap<String, String> postData = new LinkedHashMap<String, String>();
	            
	        	postData.put("what", what);
	        	
	        	CSHttpRequestUrl url = CSHttpRequest.prepare(SECTION, GET, postData);            
            request.addPostData("data", url.getData());
            request.execute(url.getUrl());
        }
        catch (Exception ex) {
            requestFailed(ex.getMessage());
        }
    }
    
    public void update(ArrayList<Integer>notifications)  {
        CSHttpRequest request = new CSHttpRequest();
        request.setHttpRequestListener(new CSHttpRequestListener() {
            public void onRequestFinished(CSHttpResponse response) {
                if (response.getSuccess()) {
                    requestFinished(response.getData());
                }            
                else {
                    requestFailed();
                }
            }
        });
        try {
            JSONArray array = new JSONArray();
            for (Integer ntId : notifications) {
	            JSONObject notification = new JSONObject();
	            notification.put("nt_id", ntId);
	            array.put(notification);
            }
            
            LinkedHashMap<String, String> postData = new LinkedHashMap<String, String>();
            postData.put("update_collection", array.toString());
            
            CSHttpRequestUrl url = CSHttpRequest.prepare(SECTION, UPDATE, postData);
            request.addPostData("data", url.getData());
            request.execute(url.getUrl());
        }
        catch (Exception ex) {
            requestFailed(ex.getMessage());
        }
    }

    private void requestFinished(String result) {
        if (mRequestListener == null) {
            return;
        }
        
        try {        
            // we got a response of some kind
            //
            JSONObject json = new JSONObject(result);
            int status = json.getInt("status");
            
            // failed on the server side
            if(status != 1)
            {
                int errorCode = json.getInt("error_code");
                mRequestListener.onRequestFinished(
                        new CSResponse<JSONArray>(errorCode, "Connectivity error. Server side"));
                return;
            }
            
            CSResponse<JSONArray> response = new CSResponse<JSONArray>(true, CSCVXI.SUCCESS, json.getJSONArray("data"));
            
            mRequestListener.onRequestFinished(response);
        }
        catch (Exception ex) {
            mRequestListener.onRequestFinished(new CSResponse<JSONArray>(CSCVXI.ERROR, ex.getMessage()));
        }
    }

    private void requestFailed() {
        requestFailed("");
    }
    
    private void requestFailed(String message) {
        // failed on the client / connectivity side
        //
        if (mRequestListener == null) {
            return;
        }
        mRequestListener.onRequestFinished(new CSResponse<JSONArray>(CSCVXI.ERROR, "Connectivity error. Client side. " + message));
    }    
}
