package ar.com.crowsoft.cscvxi.client;

public interface CSRequestListener<T> {
	 public abstract void onRequestFinished(CSResponse<T> response);
}
