package ar.com.crowsoft.cscvxi;

import java.util.Properties;

import org.apache.log4j.Logger;

public class AplicationContext {

	private static final Logger logger = Logger.getRootLogger();
	
	private static ClassFactory factory = null;
	
	public static void initialize(Properties properties) {
		logger.info("[AplicationContext] initializing.");
		factory = new ClassFactory(properties);
	}
	
	public static Object getBean(String key) throws Exception {
		if (factory != null) {
			return factory.getBean(key);
		}
		logger.error("[AplicationContext] application context not intialized.");
		return null;		
    }
}
