package ar.com.crowsoft.cscvxi;

import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Properties;
import java.lang.reflect.Type;

import org.apache.log4j.Logger;

import ar.com.crowsoft.cscvxi.service.EngineLauncher;

public class ClassFactory {

	private static final Logger log = Logger.getLogger(EngineLauncher.class);
	
	private final static String BEAN_KEY = "bean.";
	private final static String CONSTRUCTOR_KEY = ".constructor.param.";
	private final static String SINGLETON_KEY = ".singleton";
	private final static String PROPERTY_KEY = "$";
	private final static String PROPERTY_KEY_INT = "$<int>";
	
	Properties properties;
	private HashMap<String, Object> singletons = null;
	
	public ClassFactory(Properties properties) {
		this.properties = properties;
		this.singletons = new HashMap<String, Object>();
	}
	
	public Object getBean(String name) throws Exception {
		return getBean(name, null, null);
	}
	
	@SuppressWarnings("rawtypes")
	public Object getBean(String name, Class requiredType, final Object[] args) throws Exception {
		log.info("Getting object: [" + name + "] params: [" + (args != null ? args.toString() : "null") + "]");
		try {
			boolean isSingleton = getBooleanProperty(BEAN_KEY + name + SINGLETON_KEY);
			if (isSingleton) {
				if(singletons.containsKey(name))
					return singletons.get(name);
			}
			String className = properties.getProperty(BEAN_KEY + name);
			Class c= Class.forName(className);
			Object[] arguments = args != null ? args : getArguments(name);
			Constructor constructor = null;
			Constructor[] constructors = c.getDeclaredConstructors();
			
			for (int i = 0; i < constructors.length; i++) {
			    constructor = constructors[i];
			    Type[] types = constructor.getGenericParameterTypes();
			    if (types.length == arguments.length)
			    	if (arguments.length == 0)
			    		break;
			    	else
			    	{
			    		if (checkArgumentsAreTheSame(arguments, types))
			    			break;
			    	}	
			    constructor = null;
			}
			
			Object bean = null;
			if (constructor != null) 
				bean = createObject(constructor, arguments);
			
			if (isSingleton && bean != null)
				singletons.put(name, bean);
			
			return bean;
		} catch(Exception ex) {
			log.error("Failed to create object: [" + name + "] params: [" + (args != null ? args.toString() : "null") + "]");
			throw ex;
		}
	}

	private boolean checkArgumentsAreTheSame(Object[] arguments, Type[] types) {
		for (int j = 0; j < arguments.length; j++) {
			if (!types[j].equals(arguments[j].getClass()))
				if (!(types[j].equals(int.class) 
						&& arguments[j].getClass().equals(Integer.class)))
					if (!implementsInterface(arguments[j].getClass(), types[j]))
						if (!extendsClass(arguments[j].getClass(), types[j]))
							return false;
		}
		return true;
	}
	
	private boolean implementsInterface(Class<?> argumentClass, Type type) {
		Class<?>[] interfaces = argumentClass.getInterfaces();
		for (Class<?> i : interfaces) {
			if(type.equals(i))
				return true;
		}
		return false;
	}
	
	private boolean extendsClass(Class<?> argumentClass, Type type) {
		Class<?> superClass = argumentClass.getSuperclass(); 
		if (superClass == null)
			return false;
		if (type.equals(superClass))
			return true;
		return extendsClass(superClass, type);
	}
	
	private Object[] getArguments(String name) throws Exception {
		ArrayList<Object> list = new ArrayList<Object>();
		for (int i=1; true; i++) {
			String arg = properties.getProperty(BEAN_KEY + name + CONSTRUCTOR_KEY + i);
			if (arg != null) {
				if (arg.startsWith(PROPERTY_KEY))
					if (arg.startsWith(PROPERTY_KEY_INT)) 
						list.add(Integer.parseInt(properties.getProperty(arg.substring(6, arg.length()))));
					else
						list.add(properties.getProperty(arg.substring(1, arg.length())));
				else
					list.add(getBean(arg));
			}
			else
				break;
		}
		Object[] args = new Object[list.size()];
		list.toArray(args);
		return args;
	}
	
	@SuppressWarnings("rawtypes")
	private static Object createObject(Constructor constructor, Object[] arguments) throws Exception {
		try {
			return constructor.newInstance(arguments);
		} catch(Exception ex) {
			log.error("Failed to create object: [" + constructor.getName() + "] params: [" + (arguments != null ? arguments.toString() : "null") + "]");
			throw ex;
		}
	}
	
	private boolean getBooleanProperty(String key) {
		return "true".equals(properties.getProperty(key, "false"));
	}
}
