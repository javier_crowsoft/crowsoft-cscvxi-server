package ar.com.crowsoft.cscvxi.logmonitor;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.Scanner;

public class LogMonitor {

	private static BufferedReader br;

	public static void main(String[] args) {
		if (args.length < 2) {
			printUsage();
		}
		else if(args[0].equalsIgnoreCase("help"))
			printUsage();
		else if (!args[0].equals("f"))
			printUsage();
		else {
			try {
				br = new BufferedReader(new FileReader(args[1]));
				String line;
				while (true) {
						line = br.readLine();
				    if (line == null) {
				        //wait until there is more of the file for us to read
				        Thread.sleep(1000);
				    }
				    else {
				        System.out.println(line);
				    }
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
    }
	
	private static void printUsage() {
		System.out.println("Usage: LogMonitor f {full_file_name}");
        System.out.println("Presss enter to continue");
        Scanner sc = new Scanner(System.in);
        sc.nextLine();
	}
}
