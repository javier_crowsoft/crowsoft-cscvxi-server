package ar.com.crowsoft.cscvxi.service;

import java.util.Date;
import java.util.Scanner;

import org.apache.log4j.Logger;
import org.apache.commons.daemon.Daemon;
import org.apache.commons.daemon.DaemonContext;

/**
 * Launch the Engine from a variety of sources, either through a main() or invoked through
 * Apache Daemon.
 */
public class EngineLauncher implements Daemon {
    
	private static final Logger log = Logger.getLogger(EngineLauncher.class);
    
    private static EngineLauncher engineLauncherInstance = new EngineLauncher();

    private Engine engine = null;
    
    /**
     * The Java entry point.
     * @param args Command line arguments, all ignored.
     */
    public static void main(String[] args) {
    	log.debug("main called");
    	// the main routine is only here so I can also run the app from the command line
        engineLauncherInstance.initialize();

        Scanner sc = new Scanner(System.in);
        // wait until receive stop command from keyboard
        System.out.println("Enter 'stop' to halt: ");
        while(!sc.nextLine().toLowerCase().equals("stop"));

        engineLauncherInstance.terminate();
        log.debug("main exit");
    }

    /**
     * Static methods called by prunsrv to start/stop
     * the Windows service.  Pass the argument "start"
     * to start the service, and pass "stop" to
     * stop the service.
     *
     * Taken lock, stock and barrel from Christopher Pierce's blog at http://blog.platinumsolutions.com/node/234
     *
     * @param args Arguments from prunsrv command line
     **/
    public static void windowsService(String args[]) {
        String cmd = "start";
        if (args.length > 0) {
            cmd = args[0];
        }

        if ("start".equals(cmd)) {
            engineLauncherInstance.windowsStart();
        }
        else {
            engineLauncherInstance.windowsStop();
        }
    }

    public void windowsStart() {
    	log.info("cscvxi.service starting " + new Date());
        log.debug("windowsStart called");
        initialize();
        while (!engine.isStopped()) {
        	log.info("cscvxi.service started " + new Date());
            // don't return until stopped
            synchronized(this) {
                try {
                    this.wait(60000);  // wait 1 minute and check if stopped
                    log.info("cscvxi.service running " + new Date());
                }
                catch(InterruptedException ie) {
                	ie.printStackTrace();
                }
            }
        }
    }

    public void windowsStop() {
        log.debug("windowsStop called");
        terminate();
        synchronized(this) {
            // stop the start loop
            this.notify();
        }
        log.info("cscvxi.service end " + new Date());
        log.info("------------------");
    }

    // Implementing the Daemon interface is not required for Windows but is for Linux
    @Override
    public void init(DaemonContext arg0) throws Exception {
        log.debug("Daemon init");
        }

    @Override
    public void start() {
        log.debug("Daemon start");
        initialize();
    }

    @Override
    public void stop() {
        log.debug("Daemon stop");
        terminate();
    }

    @Override
    public void destroy() {
        log.debug("Daemon destroy");
    }

    /**
     * Do the work of starting the engine
     */
    private void initialize() {
    	//StdOutErrLog.tieSystemOutAndErrToLog();
        if (engine == null) {
            log.info("Starting the Engine");
            engine = new Engine();
        }
    }

    /**
     * Cleanly stop the engine.
     */
    public void terminate() {
        if (engine != null) {
            log.info("Stopping the Engine");
            engine.stop();
            log.info("Engine stopped");
        }
    }
}