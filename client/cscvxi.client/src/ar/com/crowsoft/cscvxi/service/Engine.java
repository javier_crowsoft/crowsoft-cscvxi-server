package ar.com.crowsoft.cscvxi.service;

import java.util.Date;

import org.apache.log4j.Logger;

import ar.com.crowsoft.cscvxi.CSCVXI;
import ar.com.crowsoft.cscvxi.client.CSClient;

public class Engine extends ExecutorBase implements Runnable {
	
	private static final Logger log = Logger.getLogger(EngineLauncher.class);
	   
	private enum Status {
		STOPPED,
		RUNNING
	}
	
	private Status status = Status.STOPPED;
	private Worker worker = null;
	private int timeout = 0;
	private int frequency = 0;
	
	public Engine() {
		try {
			status = Status.RUNNING;
			
			log.info("Initializing CSCVXI");
			CSCVXI.initialize();
	
			log.info("Initializing CSClient");			
			CSClient.initialize();
			
			timeout = CSClient.getConfiguration().getTimeout() * 1000;
			frequency = CSClient.getConfiguration().getFrequency() * 1000;
			executor.execute(this);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public boolean isStopped() {
		return status == Status.STOPPED;
	}

	synchronized public void stop() {
		shutdownAndAwaitTermination();
		status = Status.STOPPED;
		worker.stop();
	}

	@Override
	public void run() {
		
		try {
			
			for(;;) {
				synchronized(this) {
					if (status == Status.STOPPED)
						break;
					long startTime = new Date().getTime();
					this.worker = new Worker(this);
					this.wait(timeout);
					worker.stop();
					long endTime = new Date().getTime();
					if (endTime - startTime < frequency)
						this.wait(frequency - (endTime - startTime));
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
