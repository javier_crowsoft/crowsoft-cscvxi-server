package ar.com.crowsoft.cscvxi.service;

import java.util.ArrayList;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import ar.com.crowsoft.cscvxi.CSCVXI;
import ar.com.crowsoft.cscvxi.business.logic.OrderBL;
import ar.com.crowsoft.cscvxi.business.object.Order;
import ar.com.crowsoft.cscvxi.client.CSNotifications;
import ar.com.crowsoft.cscvxi.client.CSRequestListener;
import ar.com.crowsoft.cscvxi.client.CSResponse;

public class Worker extends ExecutorBase implements Runnable {

	private static final String CS_GET_ORDERS = "orders";
	//private static final String CS_GET_QUESTIONS = "questions";
	//private static final String CS_GET_ITEMS = "items";
	
	private static final Logger log = Logger.getLogger(EngineLauncher.class);
	
	private Object caller = null;
	
	public Worker(Object caller) {
		this.caller = caller;
		executor.execute(this);
	}
	
	@Override
	public void run() {
		getOrders();
	}
	
	private void getOrders() {
		try {
			log.info("Pulling orders");
			CSNotifications notifications = new CSNotifications();
			notifications.setRequestListener(new CSRequestListener<JSONArray>() {
				@Override
				public void onRequestFinished(CSResponse<JSONArray> response) {
					if (response.getSuccess()) {
						ArrayList<Integer> processedOrders = processNotifications(response);
						updateOrders(processedOrders);
					}
					else {
						synchronized(caller) {
							caller.notify();
						}
					}
				}					
			});
			notifications.get(CS_GET_ORDERS);
		} catch (Exception e) {
			e.printStackTrace();
		}		
	}
	
	private void updateOrders(ArrayList<Integer> processedOrders) {
		try {
			log.info("Updating orders");
			CSNotifications notifications = new CSNotifications();
			notifications.setRequestListener(new CSRequestListener<JSONArray>() {
				@Override
				public void onRequestFinished(CSResponse<JSONArray> response) {
					if (response.getSuccess()) {
						log.info("Orders updated successfully");
						JSONArray array = response.getObject();	
						try {
							log.info("Orders updated successfully " + array.getJSONObject(0).getInt("rows_affected"));
						} catch (JSONException e) {
							e.printStackTrace();
						}
					}
					else
						log.error("Update orders has failed");
					
					synchronized(caller) {
						caller.notify();
					}
				}					
			});
			notifications.update(processedOrders);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private ArrayList<Integer> processNotifications(CSResponse<JSONArray> response) {
		ArrayList<Integer> processedOrders = new ArrayList<Integer>();
		try {
			JSONArray array = response.getObject();	
			log.info("Processing " + array.length() + " orders");
			for (int i = 0; i < array.length(); i++) {
				try {
					JSONObject item = array.getJSONObject(i);
					printDebug(item);
					OrderBL orderBL = (OrderBL) CSCVXI.getBean("OrderBL");				
					Order order = orderBL.Save(item);
					if (order.getDatabaseResult().isSuccess()) {
						processedOrders.add(order.getNtId());
						log.info("Sending email meli order id: " + order.getMeliOrderId() + " db order id: " + order.getDatabaseResult().getOrderId());					
					}
					else {
						log.error("Failed to save order");
					}
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		} catch (JSONException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return processedOrders;
	}

	private void printDebug(JSONObject item) throws JSONException {
		log.info("row------------------------------------");
		
		String[] names = JSONObject.getNames(item);
		for (String name : names) {
			log.info(name + ": " + item.get(name));
		}
	}
	
	synchronized public void stop() {
		shutdownAndAwaitTermination();
	}
}
