package ar.com.crowsoft.cscvxi;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.log4j.Logger;

public class PropertiesManager {

	private static final String DEFAULT_APP_PROPERTIES_FILE = "cscvxi.properties";
	private static final String DATABASE_DEFINITION_FILE = "database.definition.file";
	private static final String DATABASE_CONNECTION_STRING = "database.connection.string";

	private static final Logger logger = Logger.getRootLogger();
	
	private static Properties properties = null;
	
	public static void initialize() throws FileNotFoundException, IOException {
		if (properties == null) {
			properties = new Properties();

			logger.info("Initializing Properties Manager...");

			// get the initial properties file that contains the location of the web application properties file
			InputStream in = (new PropertiesManager()).getClass().getClassLoader().getResourceAsStream(DEFAULT_APP_PROPERTIES_FILE);
			if (in == null) throw new FileNotFoundException("Could not find '" + DEFAULT_APP_PROPERTIES_FILE + "' file");

			PropertiesManager.load(in);
			logger.info("Base properties location: " + DEFAULT_APP_PROPERTIES_FILE);
			in.close();
			
			// get database properties
			String databasePropertiesFile = properties.getProperty(DATABASE_DEFINITION_FILE);
			try {
				logger.info("Loading database definition file: " + databasePropertiesFile);
				PropertiesManager.load((new PropertiesManager()).getClass().getClassLoader().getResourceAsStream(databasePropertiesFile));
				logger.info("Initializing Properties Manager... done");
			} catch (FileNotFoundException e) {
				logger.error("Could not load settings from: " + databasePropertiesFile);
				throw e;
			}
			validate();
		}
	}
	
	private static void load(InputStream in) throws IOException, IllegalArgumentException {
		properties.load(in);
	}
	
	public static Properties getProperties() {
		return properties;
	}

	public static String getProperty(String key) {
		return properties.getProperty(key);
	}
	
	private static void validate() {
		String connectionString = properties.getProperty(DATABASE_CONNECTION_STRING);
		if (connectionString == null) { // must have a value
			logger.warn("[PropertiesManager] validate: the database connection string is not set");
		} else { //returned a value - is it in the acceptable range?
			logger.info("[PropertiesManager] validate: properties validated successfully");
		}
	}
}
