package ar.com.crowsoft.cscvxi;

public class CSCVXI {

	public static final int SUCCESS = 0;
    public static final int ERROR = 1;

    public static void initialize() throws Exception {
    	PropertiesManager.initialize();
    	AplicationContext.initialize(PropertiesManager.getProperties());
    }
    
	public static Object getBean(String key) throws Exception {
        return AplicationContext.getBean(key);
    }
}
