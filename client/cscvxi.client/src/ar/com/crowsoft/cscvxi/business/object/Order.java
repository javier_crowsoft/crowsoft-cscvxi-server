package ar.com.crowsoft.cscvxi.business.object;

import java.math.BigDecimal;
import java.util.Date;

import ar.com.crowsoft.database.DatabaseQueryResult;

public class Order {
	
	private int orderId;
	
	private int ntId;
	private int cmiId;
	private int cmiaId;
	private String sellerNick;
	private String meilOrderId;
	private String nick;
	
	private String firstName;
	private String lastName;
	private String product;
	private String productId;
	private String strPrice;
	private String strQuantity;
	private BigDecimal price;

	private BigDecimal quantity;

	private String email;
	private String phone;
	private String city;
	private String state;

	private Date date;

	public Order(int ntId, int cmiId, int cmiaId, String sellerNick, String meliOrderId,
			String nick, String firstName, String lastName, String product,
			String productId, String strPrice, String strQuantity,
			BigDecimal price, BigDecimal quantity, String email, String phone,
			String city, String state, Date date) {

		this.ntId = ntId;
		this.cmiId = cmiId;
		this.cmiaId = cmiaId;
		this.sellerNick = sellerNick;
		this.meilOrderId = meliOrderId;
		this.nick = nick;
		this.firstName = firstName;
		this.lastName = lastName;
		this.product = product;
		this.productId = productId;
		this.strPrice = strPrice;
		this.strQuantity = strQuantity;
		this.price = price;
		this.quantity = quantity;
		this.email = email;
		this.phone = phone;
		this.city = city;
		this.state = state;
		this.date = date;
	}

	public int getNtId() {
		return ntId;
	}
	
	public int getOrderId() {
		return orderId;
	}
	
	public int getCmiId() {
		return cmiId;
	}

	public int getCmiaId() {
		return cmiaId;
	}

	public String getSellerNick() {
		return sellerNick;
	}

	public String getMeliOrderId() {
		return meilOrderId;
	}

	public String getNick() {
		return nick;
	}

	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public String getProduct() {
		return product;
	}

	public String getProductId() {
		return productId;
	}

	public String getStrPrice() {
		return strPrice;
	}

	public String getStrQuantity() {
		return strQuantity;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public BigDecimal getQuantity() {
		return quantity;
	}

	public String getEmail() {
		return email;
	}

	public String getPhone() {
		return phone;
	}

	public String getCity() {
		return city;
	}

	public String getState() {
		return state;
	}

	public Date getDate() {
		return date;
	}

	private DatabaseResult result;
	
	public DatabaseResult getDatabaseResult() {
		return result;
	}

	public void setResult(DatabaseResult result) {
		this.result = result;
	}

	public class DatabaseResult extends DatabaseQueryResult {

		private int emailId;
		
		public DatabaseResult(boolean status) {
			super(status);
		}

		public DatabaseResult(boolean status, int orderId, int emailId) {
			super(status);
			Order.this.orderId = orderId;
			this.emailId = emailId;
		}
		
		public int getOrderId() {
			return orderId;
		}

		public int getEmailId() {
			return emailId;
		}
		
	}
}
