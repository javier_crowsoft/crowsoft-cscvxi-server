package ar.com.crowsoft.cscvxi.business.logic;

import java.sql.SQLException;

import ar.com.crowsoft.cscvxi.business.object.Configuration;
import ar.com.crowsoft.cscvxi.database.IConfigurationDB;
import ar.com.crowsoft.database.StoredProcedureParamException;

public class ConfigurationBL {
	
	private IConfigurationDB configurationDb;
	
	public ConfigurationBL(IConfigurationDB config) {
		this.configurationDb = config;
	}
	
	public String getMeliUser() throws SQLException, StoredProcedureParamException {
		return configurationDb.getMeliUser();
	}
	
	public String getPassword() throws SQLException, StoredProcedureParamException {
		return configurationDb.getPassword();
	}
	
	public String getClientKey() throws SQLException, StoredProcedureParamException {
		return configurationDb.getMeliUser() + "." + configurationDb.getPassword();
	}
	
	public Configuration getConfiguration() throws SQLException, StoredProcedureParamException {
		return configurationDb.getConfiguration();
	}
}
