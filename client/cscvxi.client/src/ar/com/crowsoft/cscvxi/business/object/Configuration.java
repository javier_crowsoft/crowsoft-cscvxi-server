package ar.com.crowsoft.cscvxi.business.object;

public class Configuration {
	
	public static final int CMI_ID = 1;
	public static final int CMIA_ID = 1;
	
	private String meliUser;
	private String password;
	private int timeout;
	private int frequency;
	private boolean showDebug;
	private int idmId;
	
	public int getCmiId() {
		return CMI_ID;
	}
	public int getCmiaId() {
		return CMIA_ID;
	}
	
	public String getMeliUser() {
		return meliUser;
	}
	public void setMeliUser(String meliUser) {
		this.meliUser = meliUser;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getClientKey() {
		return this.meliUser + '.' + this.password;
	}
	public int getTimeout() {
		return timeout;
	}
	public void setTimeout(int timeout) {
		this.timeout = timeout;
	}
	public int getFrequency() {
		return frequency;
	}
	public void setFrequency(int frequency) {
		this.frequency = frequency;
	}
	public boolean getShowDebug() {
		return showDebug;
	}
	public void setShowDebug(boolean debug) {
		this.showDebug = debug;
	}
	public int getIdmId() {
		return idmId;
	}
	public void setIdmId(int idmId) {
		this.idmId = idmId;
	}
}
