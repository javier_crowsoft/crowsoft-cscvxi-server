package ar.com.crowsoft.cscvxi.business.logic;

import java.math.BigDecimal;
import java.util.Date;
import javax.xml.bind.DatatypeConverter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import ar.com.crowsoft.cscvxi.business.object.Order;
import ar.com.crowsoft.cscvxi.client.CSClient;
import ar.com.crowsoft.cscvxi.database.IOrderDB;
import ar.com.crowsoft.database.Database;

public class OrderBL {

	private IOrderDB orderDb;
	
	public OrderBL(IOrderDB order) {
		this.orderDb = order;
	}
	
	public Order Save(JSONObject notification) throws JSONException {
		
		JSONObject jOrder = new JSONObject(notification.getString("nt_message_info"));
		JSONObject buyer = jOrder.getJSONObject("buyer");
		JSONObject seller = jOrder.getJSONObject("seller");
		JSONArray items = jOrder.getJSONArray("order_items");
		JSONObject item = items.getJSONObject(0);
		JSONObject itemDescription = item.getJSONObject("item");
		
		int ntId= notification.getInt("nt_id");
		String selerNick = new Long(seller.getLong("id")).toString();
		String meliOrderId = new Long(jOrder.getLong("id")).toString();
		String nick = buyer.getString("nickname");
		String firstName = buyer.getString("first_name");
		String lastName = buyer.getString("last_name");
		String product = itemDescription.getString("title");
		String productId = itemDescription.getString("id");
		String strPrice = new Double(item.getDouble("unit_price")).toString();
		String strQuantity = new Double(item.getDouble("quantity")).toString();
		BigDecimal price = Database.getNumber(item.getDouble("unit_price"));
		BigDecimal quantity = Database.getNumber(item.getDouble("quantity"));
		String email = buyer.getString("email");
		String phone = getPhoneNumber(buyer.getJSONObject("phone"));
		String city = "";
		String state = "";
		
		Date date = DatatypeConverter.parseDateTime(jOrder.getString("date_created")).getTime();
		
		Order order = new Order(
				ntId,
				new Integer(CSClient.getConfiguration().getCmiId()),
				new Integer(CSClient.getConfiguration().getCmiaId()),
				 selerNick,  meliOrderId,
				 nick,  firstName,  lastName,  product,
				 productId,  strPrice,  strQuantity,
				 price,  quantity,  email,  phone,
				 city,  state,  date
				);
		return orderDb.Save(order);
	}

	private String getPhoneNumber(JSONObject phoneNumber) throws JSONException {
		String number = phoneNumber.optString("number");
		String areaCode = phoneNumber.optString("area_code");
		String extension = phoneNumber.optString("extension");
		return (!areaCode.isEmpty() ? "(" + areaCode + ") " : "") 
				+ (!number.isEmpty() ? number : "")
				+ (!extension.isEmpty() ? " ext: " + extension : "");
	}
}
