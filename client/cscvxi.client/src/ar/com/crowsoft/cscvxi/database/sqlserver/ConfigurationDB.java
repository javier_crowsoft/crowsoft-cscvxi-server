package ar.com.crowsoft.cscvxi.database.sqlserver;

import java.sql.*; 

import org.apache.log4j.Logger;
import ar.com.crowsoft.cscvxi.database.IConfigurationDB;
import ar.com.crowsoft.cscvxi.business.object.Configuration;
import ar.com.crowsoft.database.StoredProcedure;
import ar.com.crowsoft.database.StoredProcedureParamException;

public class ConfigurationDB extends StoredProcedure implements IConfigurationDB {

	private static final Logger logger = Logger.getRootLogger();
	
	int queryTimeOut = 60;
	
	public ConfigurationDB(String connectionString, String driverClass, int queryTimeOut) throws ClassNotFoundException, SQLException {
		Class.forName(driverClass);
        this.con = DriverManager.getConnection(connectionString);
        logger.info(this.getClass().toString() + " Connected.");
        
		this.queryTimeOut = queryTimeOut;
	}
	
	@Override
	public String getMeliUser() throws SQLException, StoredProcedureParamException {
		setProcedureName("sp_meli_user_get");
		CallableStatement call = prepareCall();
		call.setEscapeProcessing(true);
		call.setQueryTimeout(queryTimeOut);
		ResultSet rs = call.executeQuery();
		if (rs.next())
			return rs.getString("cmimeli_name");
		else
			return "";
	}

	@Override
	public String getPassword() throws SQLException, StoredProcedureParamException {
		setProcedureName("sp_meli_password_get");
		CallableStatement call = prepareCall();
		call.setEscapeProcessing(true);
		call.setQueryTimeout(queryTimeOut);
		ResultSet rs = call.executeQuery();
		if (rs.next())
			return rs.getString("cmimeli_password");
		else
			return "";
	}

	@Override
	public Configuration getConfiguration() throws SQLException, StoredProcedureParamException {
		setProcedureName("sp_meli_configuration_get");
		CallableStatement call = prepareCall();
		call.setEscapeProcessing(true);
		call.setQueryTimeout(queryTimeOut);
		ResultSet rs = call.executeQuery();
		Configuration configuration = new Configuration();
		if (rs.next()) {
			configuration.setMeliUser(rs.getString("cmimeli_name"));
			configuration.setPassword(rs.getString("cmimeli_password"));
			configuration.setTimeout(rs.getInt("cmimeli_timeout"));
			configuration.setFrequency(rs.getInt("cmimeli_frequency"));
			configuration.setShowDebug(rs.getBoolean("cmimeli_showdebug"));
			configuration.setIdmId(rs.getInt("idm_id"));
		}
		return configuration;
	}

}
