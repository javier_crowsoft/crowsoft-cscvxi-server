package ar.com.crowsoft.cscvxi.database;

import java.sql.SQLException;

import ar.com.crowsoft.cscvxi.business.object.Configuration;
import ar.com.crowsoft.database.StoredProcedureParamException;

public interface IConfigurationDB {
	public String getMeliUser() throws SQLException, StoredProcedureParamException;
	public String getPassword() throws SQLException, StoredProcedureParamException;
	public Configuration getConfiguration() throws SQLException, StoredProcedureParamException;
}
