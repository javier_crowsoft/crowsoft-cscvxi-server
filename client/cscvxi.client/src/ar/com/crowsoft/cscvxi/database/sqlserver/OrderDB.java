package ar.com.crowsoft.cscvxi.database.sqlserver;

import java.sql.CallableStatement;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import ar.com.crowsoft.cscvxi.business.object.Order;
import ar.com.crowsoft.cscvxi.database.IOrderDB;
import ar.com.crowsoft.database.StoredProcedure;

public class OrderDB extends StoredProcedure implements IOrderDB {

	private static final Logger logger = Logger.getRootLogger();
	
	int queryTimeOut = 60;
	
	public OrderDB(String connectionString, String driverClass, int queryTimeOut) throws ClassNotFoundException, SQLException {
		Class.forName(driverClass);
        this.con = DriverManager.getConnection(connectionString);
        logger.info(this.getClass().toString() + " Connected.");
        
		this.queryTimeOut = queryTimeOut;
	}
	
	@Override
	public Order Save(Order order) {
		try {
			setProcedureName("sp_srv_cvxi_ventasave");
			
			putInt("cmi_id", order.getCmiId());
			putInt("cmia_id", order.getCmiaId());
			putString("sellerNick", order.getSellerNick());
			putString("meliOrderId", order.getMeliOrderId());
			putString("nick", order.getNick());
			putString("firstName", order.getFirstName());
			putString("lastName", order.getLastName());
			putString("product", order.getProduct());
			putString("productId", order.getProductId());
			putString("strPrice", order.getStrPrice());
			putString("strQuantity", order.getStrQuantity());
			putBigDecimal("price", order.getPrice());
			putBigDecimal("quantity", order.getQuantity());
			putString("email", order.getEmail());
			putString("phone", order.getPhone());
			putString("city", order.getCity());
			putString("state", order.getState());
			putDate("date", order.getDate());
			
			CallableStatement call = prepareCall();
			call.setEscapeProcessing(true);
			call.setQueryTimeout(queryTimeOut);
			ResultSet rs = call.executeQuery();
			if (rs.next())
				order.setResult(order.new DatabaseResult(true, rs.getInt("cmiv_id"), rs.getInt("cmie_id")));
			else
				order.setResult(order.new DatabaseResult(false));
		}
		catch (Exception e) {
			e.printStackTrace();
			order.setResult(order.new DatabaseResult(false));
		}
		return order;
	}

}
