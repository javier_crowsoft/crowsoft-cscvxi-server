package ar.com.crowsoft.cscvxi.database;

import ar.com.crowsoft.cscvxi.business.object.Order;

public interface IOrderDB {

	public Order Save(Order order);
}
