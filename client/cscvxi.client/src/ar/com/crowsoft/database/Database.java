package ar.com.crowsoft.database;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;

public class Database {

	public static final int CS_NO_ID = 0;

	public static BigDecimal getNumber(double value) {
		DecimalFormat df = new DecimalFormat("#.######");
		df.setRoundingMode(RoundingMode.HALF_EVEN);
		return new BigDecimal(df.format(value));
	}
	
}
