package ar.com.crowsoft.database;

public abstract class DatabaseQueryResult {

	private boolean status = false;

	public DatabaseQueryResult(boolean status) {
		this.status = status;
	}
	
	public boolean isSuccess() {
		return status;
	}
}
