package ar.com.crowsoft.database;

public class StoredProcedureParam {
	private String name;
	private Object value;
	
	public StoredProcedureParam(String name, Object value) {
		this.name = name;
		this.value = value;
	}
	
	public String getName() {
		return name;
	}
	public Object getValue() {
		return value;
	}
}
