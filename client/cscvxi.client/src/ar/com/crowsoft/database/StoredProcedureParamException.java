package ar.com.crowsoft.database;

public class StoredProcedureParamException extends Exception {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public StoredProcedureParamException() {
		
	}
	 
	public StoredProcedureParamException(String msg) {
		super(msg);
	}
}
	 
	