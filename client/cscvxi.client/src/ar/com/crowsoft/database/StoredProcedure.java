package ar.com.crowsoft.database;

import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;

public abstract class StoredProcedure {

	private String procedureName;
	private ArrayList<StoredProcedureParam> params = new ArrayList<StoredProcedureParam>();

	protected Connection con;	
	
	public String getProcedureName() {
		return procedureName;
	}
	public void setProcedureName(String procedureName) {
		this.procedureName = procedureName;
	}
	public ArrayList<StoredProcedureParam> getParams() {
		return params;
	}

	public void putInt(String paramName, int value) {
		params.add(new StoredProcedureParam(paramName, new Integer(value)));
	}
	
	public void putString(String paramName, String value) {
		params.add(new StoredProcedureParam(paramName, value));
	}
	
	public void putDate(String paramName, Date date) {
		params.add(new StoredProcedureParam(paramName, date));
	}
	
	public void putBigDecimal(String paramName, BigDecimal value) {
		params.add(new StoredProcedureParam(paramName, value));
	}
	
	public CallableStatement prepareCall() throws SQLException, StoredProcedureParamException {
		String sqlstmt = "{call " + procedureName + getParamList(params.size()) + "}";
		CallableStatement call = con.prepareCall(sqlstmt);
		int i = 0;
		for (StoredProcedureParam p : params) {
			i++;
			if (p.getValue() instanceof Integer) {
				call.setInt(i, (Integer)p.getValue());
			}
			else if (p.getValue() instanceof String) {
				call.setString(i,  (String)p.getValue());
			}
			else if (p.getValue() instanceof Date) {
				call.setDate(i, new java.sql.Date(((Date)p.getValue()).getTime()));
			}
			else if (p.getValue() instanceof BigDecimal) {
				call.setBigDecimal(i, (BigDecimal)p.getValue());
			}
			else
				throw new StoredProcedureParamException("The datatype of parameter " + p.getName() + " is not supported by " + StoredProcedure.class.toString() + ".prepareCall");
		}
		return call;
	}
	
	private String getParamList(int size) {
		if (size > 0) {
			StringBuilder sb = new StringBuilder((size-1) * 2 + 3);
			sb.append("(");
			for(int i=0; i < size-1; i++){
				sb.append("?,");
			}
			sb.append("?)");
			return sb.toString();
		}
		else
			return "";
	}
}
