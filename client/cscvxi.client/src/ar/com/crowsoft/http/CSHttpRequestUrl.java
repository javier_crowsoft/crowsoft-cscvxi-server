package ar.com.crowsoft.http;

public class CSHttpRequestUrl {
	private String mUrl = "";
	private String mData = "";
	
	public String getUrl() {
		return mUrl;
	}
	public void setUrl(String url) {
		this.mUrl = url;
	}
	public String getData() {
		return mData;
	}
	public void setData(String data) {
		this.mData = data;
	}
}
