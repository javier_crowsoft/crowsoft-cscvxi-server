package ar.com.crowsoft.http;

class HttpRequestExecutionException extends RuntimeException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public HttpRequestExecutionException(String msg) {
		super(msg);
	}
}