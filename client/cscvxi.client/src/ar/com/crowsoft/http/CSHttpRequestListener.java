package ar.com.crowsoft.http;

public interface CSHttpRequestListener {
	public abstract void onRequestFinished(CSHttpResponse CSHttpResponse);
}
