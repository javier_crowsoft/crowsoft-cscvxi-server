if exists (select * from sysobjects where id = object_id(N'[dbo].[sp_meli_configuration_get]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[sp_meli_configuration_get]

go

create procedure sp_meli_configuration_get 
as

begin

  set nocount on

	select cmimeli_name, cmimeli_password, cmimeli_timeout, cmimeli_frequency, cmimeli_showdebug, idm_id from ComunidadInternetMeli

end