if exists (select * from sysobjects where id = object_id(N'[dbo].[sp_meli_user_get]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[sp_meli_user_get]

go

create procedure sp_meli_user_get 
as

begin

  set nocount on

	select cmimeli_name from ComunidadInternetMeli

end