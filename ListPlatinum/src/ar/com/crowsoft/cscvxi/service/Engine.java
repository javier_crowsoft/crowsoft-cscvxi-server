package ar.com.crowsoft.cscvxi.service;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import org.apache.log4j.Logger;

import ar.com.crowsoft.cscvxi.CSCVXI;
import ar.com.crowsoft.cscvxi.listplatinum.ListPlatinum;

public class Engine extends ExecutorBase implements Runnable {
	
	private static final Logger log = Logger.getLogger(EngineLauncher.class);
	
	ArrayList<String> links = new ArrayList<String>();
	ArrayList<String> profileLinks = new ArrayList<String>();
	HashMap<String, String> nickLinks = new HashMap<String, String>();
	ArrayList<String> nicks = new ArrayList<String>();
	   
	private enum Status {
		STOPPED,
		RUNNING
	}
	
	private Status status = Status.STOPPED;
	private Worker worker = null;
	private int timeout = 0;
	private int frequency = 0;
	
	public Engine() {
		try {
			status = Status.RUNNING;
			
			log.info("Initializing CSCVXI");
			CSCVXI.initialize();
	
			log.info("Initializing CSClient");			
		
			timeout = ListPlatinum.getTimeout() * 1000;
			frequency = ListPlatinum.getFrequency() * 1000;
			executor.execute(this);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public boolean isStopped() {
		return status == Status.STOPPED;
	}

	synchronized public void stop() {
		shutdownAndAwaitTermination();
		status = Status.STOPPED;
		worker.stop();
	}

	@Override
	public void run() {
		String toSearch[] = {"Cartera Three", "Cable FTP UTP", "Cortina", "Cargardor de bateria Dell", "Ventilador de techo", "Auriculares Sony", "Mi peque�o pony", "Barbie", "Disney Cars", "Celular Nokia", "Celular Samsung", "Lavarropas", "Plancha", "iPhone", "Guitarra electrica", "GPS", "Reloj", "Contadora De Dinero", "Macbook pro", "Fotocopiadora", "Sierra Circular De Pie", "Compresor Aire", "ssd", "hp 1010", "mesa ratona", "bicicleta", "campera de cuero", "lcd 42", "monitor 23", "atomlux", "botas", "silla pc", "pila AA", "Repuesto motor Renault", "Repuesto motor Fiat", "Repuesto motor Ford", "Camara digital 10 mega pixels", "Bomba sumergible", "Balanza electronica", "Estacion meteorologica", "Osciloscopio", "Tester digital"};
		//String toSearch[] = {"bolsa pasta", "Cadeira Escrit�rio", "Memoria ram", "cartucho hp 122", "mesa oficina", "mesa escritorio", "Dvd automotivo", "Gravador externo", "Carrinho Umbrella", "Cozinha", "Cortina", "Jarra El�ctrica", "Cart�o Micro Sd 4gb", "Cable FTP UTP", "Fonte P/ Semp", "Ventilador de techo", "Auricular", "Microfones", "Mi peque�o pony", "Barbie", "Disney Cars", "Celular Nokia", "Celular Samsung", "Lavarropas", "Plancha", "iPhone", "Guitarra electrica", "GPS", "Reloj", "Contadora De Dinero", "Macbook pro", "Fotocopiadora", "Sierra Circular De Pie", "Compresor Aire", "ssd", "hp 1010", "mesa ratona", "bicicleta", "campera de cuero", "lcd 42", "monitor 23", "atomlux", "botas", "silla pc", "pila AA", "Repuesto motor Renault", "Repuesto motor Fiat", "Repuesto motor Ford", "Camara Nikon", "Bomba sumergible", "Balanza electronica", "Estacion meteorologica", "Osciloscopio", "Tester digital"};
		
		//String toSearch[] = {"ssd", "hp 1010"};
		try {
			
			for (String word : toSearch) {
				synchronized(this) {
					if (status == Status.STOPPED)
						break;
					long startTime = new Date().getTime();
					this.worker = new Worker(this, Worker.SITE_URL + word, Worker.ACTION_LIST_ITEMS);
					this.wait(timeout);
					worker.stop();
					
					for (String link : worker.getLinks()) {
						links.add(link);
					}
					
					long endTime = new Date().getTime();
					if (endTime - startTime < frequency)
						this.wait(frequency - (endTime - startTime));
				}
			}
			
			for (String link : links) {
				synchronized(this) {
					if (status == Status.STOPPED)
						break;
					long startTime = new Date().getTime();
					this.worker = new Worker(this, link, Worker.ACTION_GET_SELLER_LINK);
					this.wait(timeout);
					worker.stop();
					
					for (String profileLink : worker.getLinks()) {
						profileLinks.add(profileLink);
					}
					
					long endTime = new Date().getTime();
					if (endTime - startTime < frequency)
						this.wait(frequency - (endTime - startTime));
				}
			}

			for (String profileLink : profileLinks) {
				if (profileLink.contains("\"")) {
					profileLink = profileLink.substring(0, profileLink.indexOf("\"") - 1);
				}
				if (!nickLinks.containsKey(profileLink))
					nickLinks.put(profileLink, profileLink);
			}
			
			for(String profileLink : nickLinks.values()) {
				synchronized(this) {
					if (status == Status.STOPPED)
						break;
					long startTime = new Date().getTime();
					this.worker = new Worker(this, profileLink, Worker.ACTION_GET_SELLER);
					this.wait(timeout);
					worker.stop();

					for (String nick : worker.getLinks()) {
						nicks.add(nick);
					}
					
					long endTime = new Date().getTime();
					if (endTime - startTime < frequency)
						this.wait(frequency - (endTime - startTime));
				}
			}
			
			saveNicksToFile();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void saveNicksToFile() throws FileNotFoundException, UnsupportedEncodingException {
		PrintWriter writer = new PrintWriter("./output.txt", "UTF-8");
		for(String nick : nicks) {
			writer.println(nick + "|http://www." + nick.replaceAll("_", "\\-").replaceAll("\\.", "\\-") + Worker.URL_TAIL + "|https://www.google.com/search?q=" + nick);
		}
		writer.close();
	}
}
