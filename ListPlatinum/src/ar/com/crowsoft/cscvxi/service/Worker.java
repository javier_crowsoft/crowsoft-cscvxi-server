package ar.com.crowsoft.cscvxi.service;

import java.util.ArrayList;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import ar.com.crowsoft.cscvxi.listplatinum.MercadoLibreListItems;
import ar.com.crowsoft.cscvxi.listplatinum.CSRequestListener;
import ar.com.crowsoft.cscvxi.listplatinum.CSResponse;

public class Worker extends ExecutorBase implements Runnable {

	private static final Logger log = Logger.getLogger(EngineLauncher.class);
	
	public static final String ACTION_LIST_ITEMS = "ITEMS";
	public static final String ACTION_GET_SELLER_LINK = "SELLER-LINK";
	public static final String ACTION_GET_SELLER = "SELLER";
	/*
	public static final String SITE_URL = "http://listado.mercadolibre.com.ar/";
	public static final String PRODUCT_URL = "href=\"http://articulo.mercadolibre.com.ar/";
	public static final String URL_TAIL = ".com.ar";
	public static final String PROFILE_URL = "http://www.mercadolibre.com.ar/jm/profile?id";
	public static final String MORE_DATA_TEXT = "datos del vendedor";
	public static final String REPUTATION_TEXT = "<b>Reputación de:</b>";
	public static final int REPUTATION_TEXT_LENGHT = 22;
	*/
	/*
	public static final String SITE_URL = "http://listado.mercadolibre.com.mx/";
	public static final String PRODUCT_URL = "href=\"http://articulo.mercadolibre.com.mx/";
	public static final String URL_TAIL = ".com.mx";
	public static final String PROFILE_URL = "http://www.mercadolibre.com.mx/jm/profile?id";
	public static final String MORE_DATA_TEXT = "datos del vendedor";
	public static final String REPUTATION_TEXT = "<b>Reputación de:</b>";
	public static final int REPUTATION_TEXT_LENGHT = 22;
	*/
	public static final String SITE_URL = "http://listado.mercadolibre.cl/";
	public static final String PRODUCT_URL = "href=\"http://articulo.mercadolibre.cl/";
	public static final String URL_TAIL = ".cl";
	public static final String PROFILE_URL = "http://www.mercadolibre.cl/jm/profile?id";
	public static final String MORE_DATA_TEXT = "datos del vendedor";
	public static final String REPUTATION_TEXT = "<b>Reputación de:</b>";
	public static final int REPUTATION_TEXT_LENGHT = 22;
	/*	
	public static final String SITE_URL = "http://lista.mercadolivre.com.br/";
	public static final String PRODUCT_URL = "href=\"http://produto.mercadolivre.com.br/";
	public static final String URL_TAIL = ".com.br";
	public static final String PROFILE_URL = "http://www.mercadolivre.com.br/jm/profile?id=";
	public static final String MORE_DATA_TEXT = "detalhes do vendedor";
	public static final String REPUTATION_TEXT = "<b>Reputação de</b>";
	public static final int REPUTATION_TEXT_LENGHT = 20;
	*/	
	private Object caller = null;
	private String link = "";
	private String action = "";
	ArrayList<String> links = new ArrayList<String>();
	
	public Worker(Object caller, String link, String action) {
		this.caller = caller;
		this.link = link;
		this.action = action;
		executor.execute(this);
	}
	
	@Override
	public void run() {
		getUrl();
	}
	
	private void getUrl() {
		try {
			log.info("Pulling url: " + link);
			MercadoLibreListItems items = new MercadoLibreListItems();
			items.setRequestListener(new CSRequestListener<String>() {
				@Override
				public void onRequestFinished(CSResponse<String> response) {
					if (response.getSuccess()) {
						if (action.equals(Worker.ACTION_LIST_ITEMS))
							processListItems(response);
						else if (action.equals(Worker.ACTION_GET_SELLER_LINK))
							processGetSellerLink(response);						
						else if (action.equals(Worker.ACTION_GET_SELLER))
							processGetSeller(response);						
					}
					synchronized(caller) {
						caller.notify();
					}
				}
			});
			items.get(link);
		} catch (Exception e) {
			e.printStackTrace();
		}		
	}
	
	private void processListItems(CSResponse<String> response) {
		try {
			String data = response.getObject();
			
			log.info("Processing " + data);
			
			int i = -1;
			int from = data.indexOf("<ol id=\"searchResults\" class=\"list-view\">");
			if (from < 0)
				return;
			do {
				i = data.indexOf("ch-ico mercadolider-platinum", from + 1);
				if (i > 0) {
					String block = data.substring(from + 1, i);
					int hrefStart = block.indexOf(Worker.PRODUCT_URL);
					int hrefEnd = block.indexOf(">", hrefStart);
					String link = block.substring(hrefStart, hrefEnd);
					
					if (link.length() > 8)
						links.add(link.substring(6, link.length() -1));
					
					log.info("link: " + link);
					
					from = i;
				}
			} while(i > 0);			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void processGetSellerLink(CSResponse<String> response) {
		try {
			String data = response.getObject();
			int i = data.indexOf(Worker.MORE_DATA_TEXT);
			data = data.substring(0, i);
			int j = data.indexOf(Worker.PROFILE_URL);
			data = data.substring(j);
			i = data.indexOf("'");
			String link = data.substring(0, i);
			links.add(link);
			log.info("Processing " + link);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void processGetSeller(CSResponse<String> response) {
		try {
			String data = response.getObject();
			int i = data.indexOf(Worker.REPUTATION_TEXT);
			data = data.substring(i + Worker.REPUTATION_TEXT_LENGHT);
			int j = data.indexOf("</span>");
			String nick = data.substring(0, j);
			links.add(nick);
			log.info("Processing " + nick);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	synchronized public void stop() {
		shutdownAndAwaitTermination();
	}
	
	public ArrayList<String> getLinks() {
		return links;
	}
}
