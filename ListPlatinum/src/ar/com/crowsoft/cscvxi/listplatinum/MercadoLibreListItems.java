package ar.com.crowsoft.cscvxi.listplatinum;

import ar.com.crowsoft.cscvxi.CSCVXI;
import ar.com.crowsoft.http.CSHttpRequest;
import ar.com.crowsoft.http.CSHttpRequestListener;
import ar.com.crowsoft.http.CSHttpResponse;

public class MercadoLibreListItems {
    
    private CSRequestListener<String> mRequestListener;

    public void setRequestListener(CSRequestListener<String> requestListener) {
        mRequestListener = requestListener;
    }
    
    public void get(String link) {
        CSHttpRequest request = new CSHttpRequest();
        request.setHttpRequestListener(new CSHttpRequestListener() {
            public void onRequestFinished(CSHttpResponse response) {
                if (response.getSuccess()) {
                    requestFinished(response.getData());
                }            
                else {
                    requestFailed();
                }
            }
        });
        try {           
            request.execute(link);
        }
        catch (Exception ex) {
            requestFailed(ex.getMessage());
        }
    }
    
    private void requestFinished(String result) {
        if (mRequestListener == null) {
            return;
        }
        
        try {        
            CSResponse<String> response = new CSResponse<String>(true, CSCVXI.SUCCESS, result);
            
            mRequestListener.onRequestFinished(response);
        }
        catch (Exception ex) {
            mRequestListener.onRequestFinished(new CSResponse<String>(CSCVXI.ERROR, ex.getMessage()));
        }
    }

    private void requestFailed() {
        requestFailed("");
    }
    
    private void requestFailed(String message) {
        // failed on the client / connectivity side
        //
        if (mRequestListener == null) {
            return;
        }
        mRequestListener.onRequestFinished(new CSResponse<String>(CSCVXI.ERROR, "Connectivity error. Client side. " + message));
    }    
}
