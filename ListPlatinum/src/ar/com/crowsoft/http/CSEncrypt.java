package ar.com.crowsoft.http;

import java.security.MessageDigest;
import org.apache.commons.codec.binary.Base64;

public class CSEncrypt {

	public static String md5(String value) throws Exception {
		MessageDigest algorithm = MessageDigest.getInstance("MD5");
		algorithm.reset();
		algorithm.update(value.getBytes());
		byte messageDigest[] = algorithm.digest();
	            
		StringBuffer hexString = new StringBuffer();
		
		for (int i = 0; i < messageDigest.length; i++) {
			hexString.append(Integer.toString((messageDigest[i] & 0xff) + 0x100, 16).substring(1));
		}
		
		return hexString.toString();
	}
	
	/**
     * Encodes the string 'in' using 'flags'.  Asserts that decoding
     * gives the same string.  Returns the encoded string.
     */
    public static String encodeToString(String in) throws Exception {
        /*int[] flagses = { Base64.DEFAULT,
                Base64.NO_PADDING,
                Base64.NO_WRAP,
                Base64.NO_PADDING | Base64.NO_WRAP,
                Base64.CRLF,
                Base64.URL_SAFE };*/
        //String b64 = Base64.encodeToString(in.getBytes(), Base64.NO_WRAP);
        String b64 = new String(Base64.encodeBase64(in.getBytes()));
        return b64;
    }
}
