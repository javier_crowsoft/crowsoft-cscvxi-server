package ar.com.crowsoft.http;

import ar.com.crowsoft.cscvxi.CSCVXI;

public class CSHttpResponse {

	private Boolean mSuccess;
	private int mErrorCode = CSCVXI.ERROR;
	private String mData;
	
	public Boolean getSuccess() {
		return mSuccess;
	}

	public int getErrorCode() {
		return mErrorCode;
	}

	public String getData() {
		return mData;
	}
	
	public CSHttpResponse(Boolean success, int errorCode, String data) {
		mSuccess = success;
		mErrorCode = errorCode;
		mData = data;
	}

	public CSHttpResponse(int errorCode, String message) {
		mSuccess = false;
		mErrorCode = errorCode;
		mData = message;
	}
}
