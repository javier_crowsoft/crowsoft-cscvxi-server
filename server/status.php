<?php

require './melisdk/meli.php';
require './config.php';
require './database.php';
require './user.php';
require './util.php';

	cs_db_connect($cs_server, $cs_user, $cs_password, $cs_database);

	$sqlstmt = "select count(*) as count from customer";
	$result = cs_db_query($sqlstmt);
	if ($row = $result->fetch_assoc()) {
		$users = $row['count'];
	}
	else {
		$users = 0;
	}	

	$sqlstmt = "select count(*) as count from notification";
	$result = cs_db_query($sqlstmt);
	if ($row = $result->fetch_assoc()) {
		$notifications = $row['count'];
	}
	else {
		$notifications = 0;
	}
	
	h2("CrowSoft CSCVXI");
	h1("Status");
	
	p("Date", date("l, F j, Y h:m:s a", strtotime("+8 hours")));
	p("Users", $users);
	p("Notifications", $notifications);
	
	p("Last 20 orders");
	
	$sqlstmt = "SELECT * FROM notification WHERE nt_topic = 'orders' ORDER BY nt_id desc LIMIT 20 ";
	$result = cs_db_query($sqlstmt);
	$printCol = true;
	
	echo "<table id='gradient-style' summary='Meeting Results'>";
	while($row = $result->fetch_assoc()) {
		
		if($printCol) {
			echo "<thead><tr>";
			foreach($row as $col => $value) {
				if (!($col == "nt_message_info" || $col == "nt_message"))
					echo "<th scope='col'>$col</th>";
			}
			echo "</tr></thead><tbody>";
		}
		$printCol = false;
		
		echo "<tr>";
		foreach($row as $col => $value) {
			if (!($col == "nt_message_info" || $col == "nt_message"))
				echo "<td>$value</td>";
		}
		
		echo "</tr>";
		echo "<tr class='details'>";
		echo "<td colspan='20'>".indent($row['nt_message'])."</td>";
		echo "</tr>";
		echo "<tr>";
		echo "<td colspan='20'>".indent($row['nt_message_info'])."<br /><br /></td>";
		echo "</tr>";
	}
	echo "</tbody></table>";
	
?>

<!doctype html>
<html>
  <head>
	<meta charset="UTF-8"/>
    <title>CrowSoft CSCVXI Status</title>
    <link href="styles.css" rel="stylesheet" type="text/css">
  </head>
  <body>
  </body>
</html>