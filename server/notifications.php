<?php

require './melisdk/meli.php';
require './config.php';
require './database.php';
require './user.php';
require './util.php';

	// Create our Application instance (replace this with your appId and secret).
	$meli = new Meli(array(
			'appId'  	=> '7946554398015919',
			'secret' 	=> 'PHm3qIGGISdVCoDKfuJ1FOOguCt8mKxi',
	));
	
	$meli->initConnect();

	cs_db_connect($cs_server, $cs_user, $cs_password, $cs_database);
	
	$message = isset($_GET['debug']) ? str_replace("\\", "", $_GET['debug']) : file_get_contents('php://input');

	if ($in_debug) {
?>

<!doctype html>
<html>
	<head>
		<meta charset="UTF-8"/>
		<title>CrowSoft CSCVXI Notifications (debug)</title>
	</head>
	<body>
		<h2>CrowSoft CSCVXI</h2>
		<h1>Notifications (debug)</h1>
<?php 
	}	

	dbg("<p>Message: $message</p>");
	
	$json=json_decode($message, true);
	dbg("<p>JSON of message: " . dbg_dump($json) . "</p>");
	$user_id = $json['user_id'];
	$topic = $json['topic'];
	$resource = $json['resource'];
	
	dbg("<p>user_id: $user_id</p>");
	dbg("<p>topic: $topic</p>");
	dbg("<p>resource: $resource</p>");

	$cu_id = cs_user_get_cu_id_from_meli_user_id($user_id);
	
	$sqlstmt = "insert into notification (nt_meli_user_id, nt_topic, nt_resource, nt_message, nt_processed, cu_id)"
			    . " values(?,?,?,?,0,?)";
	$qparams = array();
	$qparams[] = array("s" => $user_id);
	$qparams[] = array("s" => $topic);
	$qparams[] = array("s" => $resource);
	$qparams[] = array("s" => $message);
	$qparams[] = array("i" => $cu_id);
	$id = cs_db_insert_and_return_id($sqlstmt, $qparams);
	
	$access_token_in_db = cs_user_get_access_token($user_id);
	
	$meli->setAccessToken($access_token_in_db);
	$message = $meli->getWithAccessToken($resource);
	
	// check if the access token has been updated
	if ($user_id) {

		cs_user_check_token($meli, $access_token_in_db, $user_id);
		/*
		$user = $meli->getWithAccessToken('/users/me');
		
		$access_token = $meli->getAccessToken();
		if (is_a_valid_access_token($access_token)) {
			if ($access_token_in_db['value'] !== $access_token['value']) {
				
				dbg("<p>Updating token</p>");
				
		    	$user_code = $user['json']['nickname'];
		    	$user_name = $user['json']['first_name'].' '.$user['json']['last_name'];
		    	$user_info = json_encode($user['json']);
				cs_user_update_token($user_code, $user_name, $user_id, $user_info, $access_token['value'], $access_token['expires'], $access_token['scope'], $access_token['refresh_token']);
			}
		} 
		else 
			dbg("<p>invalid access token</p>");
		*/
	}
		
	dbg("<p>message_info:" . dbg_dump($message) . "</p>");
	dbg("<p>access_token_in_db:" . dbg_dump($access_token_in_db) . "</p>");
		
	$message = json_encode($message['json']);
	
	$sqlstmt = "update notification set nt_message_info = ? where nt_id = ?";
	$qparams = array();
	$qparams[] = array("s" => $message);
	$qparams[] = array("i" => $id);
	cs_db_query($sqlstmt, $qparams);
	
	dbg("<p>done</p>");

	if ($in_debug) {
?>

	</body>
</html>

<?php 
	}	

	/*
	function is_a_valid_access_token($access_token) {
		if(!isset($access_token))
			return false;
		if($access_token == null)
			return false;
		if(!isset($access_token['value']))
			return false;
		if ($access_token['value'] == null)
			return false;
		if ($access_token['value'] == "")
			return false;
		return true;
	}
	*/	
?>