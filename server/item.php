<?php

require './melisdk/meli.php';

	function cs_item_get_items($cu_id) {
		// Create our Application instance (replace this with your appId and secret).
		$meli = new Meli(array(
				'appId'  	=> '7946554398015919',
				'secret' 	=> 'PHm3qIGGISdVCoDKfuJ1FOOguCt8mKxi',
		));
		
		$meli->initConnect();
	
		$offset = 0;
		
		while(true) {
			
			$user_id = cs_user_get_meli_user_id_from_cu_id($cu_id);
			$access_token_in_db = cs_user_get_access_token($user_id);
			$resource = "/users/$user_id/items/search";
				
			$meli->setAccessToken($access_token_in_db);
			$message = $meli->getWithAccessToken($resource, array("offset" => $offset));
			
			// check if the access token has been updated
			if ($user_id) {
				cs_user_check_token($meli, $access_token_in_db, $user_id);
			}
			
			$body = json_decode($message['body']);			
			$results = $body->results;
			$total = $body->paging->total;
			$total = is_numeric($total) ? (int)$total : 0;
				
			foreach($results as $item) {

				$item_db = search_item_in_db($item, $cu_id);
				
				if ($item_db['is_active']) {
				
					$resource = "/items/$item";
					
					try {
			    		$item = $meli->get($resource);
			    	} catch(Exception $e) {
			    		die;
			    	}
					
			    	$item = json_decode($item['body']);

			    	if ($item_db['it_id'] == 0) {
			    	
						$sqlstmt = "insert item (it_meli_item_id, it_meli_title, it_meli_subtitle, it_meli_price, it_meli_base_price, it_meli_start_time, it_meli_stop_time, it_meli_available_quantity, it_meli_initial_quantity, it_meli_sold_quantity, it_meli_status, cu_id)"
									. "values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
						$qparams = array();
						$qparams[] = array("s" => $item->id);
						$qparams[] = array("s" => $item->title);
						$qparams[] = array("s" => $item->subtitle);
						$qparams[] = array("s" => $item->price);
						$qparams[] = array("s" => $item->base_price);
						$qparams[] = array("s" => $item->start_time);
						$qparams[] = array("s" => $item->stop_time);
						$qparams[] = array("s" => $item->available_quantity);
						$qparams[] = array("s" => $item->initial_quantity);
						$qparams[] = array("s" => $item->sold_quantity);
						$qparams[] = array("s" => $item->status);
						$qparams[] = array("s" => $cu_id);
						
						cs_db_query($sqlstmt, $qparams);
						
						//dbg("<p>inserted $item->id");
					}
					else {

						$sent = $item->status == 'active' ? 0 : 1;
						
						$sqlstmt = "update item set it_meli_available_quantity = ?, it_meli_sold_quantity = ?, sent = ? where it_id = ? and cu_id = ?";
						$qparams = array();
						$qparams[] = array("s" => $item->available_quantity);
						$qparams[] = array("s" => $item->sold_quantity);
						$qparams[] = array("s" => $sent);
						$qparams[] = array("i" => $item_db['it_id']);
						$qparams[] = array("i" => $cu_id);
						
						cs_db_query($sqlstmt, $qparams);
						
						//dbg("<p>updated $item->id");
					}				
				}
			}
			
			if ($total < $offset)
				break;
				
			$offset += 50;
		}
	}
	
	function search_item_in_db($item, $cu_id) {
		$sqlstmt = "select it_id, it_meli_status from item where it_meli_item_id = ? and cu_id = ?";
		$qparams = array();
		$qparams[] = array("s" => $item);
		$qparams[] = array("i" => $cu_id);		
		
		$result = cs_db_query($sqlstmt, $qparams);
		
    	if ($row = $result->fetch_assoc()) {
    		return array("is_active" => $row['it_meli_status'] != 'closed', "it_id" => $row['it_id']);
    	}
    	else {
    		return array("is_active" => true, "it_id" => 0);
    	}
    	
	}
?>