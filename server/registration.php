<?php

if ($_REQUEST['showcode'] == 1) {

echo "<p>Message: ".$_REQUEST['code']."</p>";
die();

}

require './melisdk/meli.php';
require './config.php';
require './database.php';
require './user.php';
require './util.php';

// Create our Application instance (replace this with your appId and secret).
$meli = new Meli(array(
	'appId'  	=> '7946554398015919',
	'secret' 	=> 'PHm3qIGGISdVCoDKfuJ1FOOguCt8mKxi',
));

$user_id = $meli->initConnect();

// Login or logout url will be needed depending on current user state.
if ($user_id) {
  $user = $meli->getWithAccessToken('/users/me');
}

?>
<!doctype html>
<html>
  <head>
	<meta charset="UTF-8"/>
    <title>CrowSoft CSCVXI Login</title>
    <link href="styles.css" rel="stylesheet" type="text/css">
  </head>
  <body>
    <h2>CrowSoft CSCVXI</h2>
    <h1>Registration</h1>
    <?php 
    
    //echo "<p>Message: ".$_REQUEST['code']."</p>";
    
    dbg("<p>Auth Code: " . dbg_dump($meli->getAccessToken()) . "</p>"); 
    
    ?>
    
    <?php 
    dbg("<p>connecting");    
    cs_db_connect($cs_server, $cs_user, $cs_password, $cs_database);
    dbg("<p>connected");
	?>
	
    <?php if ($user_id): ?>
    
    	<?php 
    		dbg("<p>Inserting/Updating user</p>");
	    	$access_token = $meli->getAccessToken();
	    	$user_code = $user['json']['nickname'];
	    	$user_name = $user['json']['first_name'].' '.$user['json']['last_name'];
	    	$user_info = json_encode($user['json']);
	    	cs_user_update_token($user_code, $user_name, $user_id, $user_info, $access_token['value'], $access_token['expires'], $access_token['scope'], $access_token['refresh_token']);
	    	dbg("<p>Inserting/Updating user done</p>");
	    	
    	?>
    <p>Hello <b><?php echo $user['json']['first_name']  ?></b> &nbsp;&nbsp;&nbsp;&nbsp;<a class="btn primary" href="<?php echo $meli->getLogoutUrl(); ?>">Logout</a></p>
    
    <?php dbg("<p>User info: " . dbg_dump($user['json']) . "</p>") ?>
      
      <form>
      		
	  </form>
		
		<?php 
			if(isset($_POST['password'])) {
	    	
				$error = cs_user_update_password($_POST['password'], $_POST['password2'], $user_id);
	    	
		    	if ($error != "") {
		    		pe($error);
		    	}
		    }
			if (cs_user_must_set_password($user_id))
				pe("You must set a password to be able to get notifications");
				
		?>
      
      <form method="post">
      	<h2>Password</h2>
      	<table width="100%">
      		<tr><td><label class="label" for="password">Password</label><input class="textfield" name="password" type="password" size="80"></td><td width="80%"></td></tr>
      		<tr><td><label class="label" for="password2">Repeat password</label><input class="textfield" name="password2" type="password" size="80"></td><td width="80%"></td></tr>
      	</table>
      	<br />
      	<input type="submit" class="btn primary large" value="submit"/>
      </form>
    <?php else: ?>
      <div>
       <p> Login using OAuth 2.0: </p>
        <a class="btn primary large" href="<?php echo $meli->getLoginUrl(array('redirect_uri' => 'http://www.crowsoft.com.ar/cscvxi/registration.php')); ?>">Login with MercadoLibre</a>
        <p><?php dbg($meli->getLoginUrl()); ?></p>
      </div>
    <?php endif ?>
    
  </body>
</html>