<?php

require "./error.php";
require './config.php';
require './database.php';
require './user.php';
require './util.php';
require './item.php';

define('ITEMS_ROW_LIMIT', 200);
define('MAX_SENT', 50);

?>

<!doctype html>
<html>
	<head>
		<meta charset="UTF-8"/>
		<title>CrowSoft CSCVXI Items List (debug)</title>
	</head>
	<body>
		<h2>CrowSoft CSCVXI</h2>
		<h1>Items List (debug)</h1>

<?php

	$in_debug = true;

    // get user
    $login = $_GET['l'];

    cs_db_connect($cs_server, $cs_user, $cs_password, $cs_database);
     
    $cu_id = cs_user_get_cu_id($login);
    
    $response = new stdClass;
    
	try {
		$response->data = get_items($cu_id);
		$response->status = 1;
	} catch(Exception $e) {
		$response->error_code = CS_UNEXPECTED_ERROR;
		$response->status = 0;
	}
    
    $response = json_encode($response);
    echo indent($response);
    
    //-------------------------------------------------------------------------
    // functions
    //-------------------------------------------------------------------------
    
    function update_sent($rows, $table, $id_column) {
    	$sqlstmt = "update $table set sent = sent + 1 where $id_column = ?";
    	
    	foreach($rows as $row) {
    		$qparams = array();
    		$qparams[] = array("i" => $row[$id_column]);
    		cs_db_query($sqlstmt, $qparams);
    	}
    }
    
    function get_items($cu_id) {
    	cs_item_get_items($cu_id);
    	
    	$sqlstmt = "select * from item where sent = 0 and cu_id = ? limit ?";
    	$qparams = array();
    	$qparams[] = array("i" => $cu_id);
    	$qparams[] = array("i" => ITEMS_ROW_LIMIT);
    	$result = cs_db_query($sqlstmt, $qparams);
    	
    	$rows = array();
    	
    	while ($row = $result->fetch_assoc()) {
    		$rows[] = $row;
    	}
    	update_sent($rows, "item", "it_id");
    	
    	dbg("<p>".dbg_dump($qparams));
    	
    	return $rows;
    }

?>
	</body>
</html>
