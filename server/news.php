<?php

require "./error.php";
require './config.php';
require './database.php';
require './user.php';
require './util.php';

	define("CS_PARAM_WHAT", "what");
	define("CS_PARAM_UPDATE_COLLECTION", "update_collection");

	define('NOTIFICATION_ROW_LIMIT', 20);
	define('ITEMS_ROW_LIMIT', 200);
	define('MAX_SENT', 50);
	
	// response object
	$response = new stdClass;
	$response->status = 0;
	$response->error_code = CS_NO_ERROR;
	
	// status of the message
	$invalid_message = false;
	
	// fixed keys
	$KEYS = "|section|action|signature|tail|";
	
	// standard variables in a request 
	$nonce = "";
	$timestamp = 0;
	$p = "";
	$section = "";
	$action = "";
	$signature = "";

	//-------------------------------------------------------------------------
	// read parameters
	//-------------------------------------------------------------------------
	
	// post data in this request
    $data = explode("&", base64_decode($_POST['data']));
    
    $debug_out = new stdClass;
    $debug_out->data = $_POST['data'];
    $debug_out->decoded_data = base64_decode($_POST['data']);

    // get the url for this service
    $url = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
    
    $debug_out->url = $url;
    
    // get service and standard variables
    foreach ($data as $e) {
    	$element = explode("=", $e);
        
    	// service variables
        if (strpos($KEYS, $element[0]) === false) {
    		$params[] = $e;
    		$name_params[$element[0]] = unescape($element[1]);
		}
		
		// standard variables
    	switch ($element[0]) {
    		case "nonce" :		$nonce		= $element[1]; break;
    		case "timestamp" : 	$timestamp 	= $element[1]; break;
    		case "p" : 			$p 			= $element[1]; break;
    		case "section" : 	$section 	= $e; break;
    		case "action" : 	$action 	= $e; break;
    		case "signature" : 	$signature 	= $e; break;
    	}
    	
    	$debug_out->params[] = $element[0] . " : " . $element[1];
    }
    
    $debug_out->vars->nonce = $nonce;
    $debug_out->vars->timestamp = $timestamp;
    $debug_out->vars->p = $p;
    $debug_out->vars->section = $section;
    $debug_out->vars->action = $action;
    $debug_out->vars->signature = $signature;
    
    // sort params so we can recreate section, action and signature
    sort($params);
    foreach ($params as $e) {
    	$element = explode("=", $e);
    	$debug_out->sorted_params[] = $element[0] . " : " . $e;
    }

    foreach ($name_params as $pkey => $pvalue) {
    	$debug_out->name_params[] = "$pkey : $pvalue";
    }
    
    // get user
    $login = $_GET['l'];

    //-------------------------------------------------------------------------
    // create signatures for all kind of messages
    //-------------------------------------------------------------------------
    
    // generate client key
    $clientKey = $login . "." . $p;
    
    // check if we need to include debug info into the response object
    $debug = $_GET['debug'];
    $debug = isset($debug) ? $debug == "y" : false;
    
    $debug_out->clientkey = $clientKey;
    
    // standard keys for this service
    $section_key = md5("notifications-$clientKey");
    $get_key = md5("notifications-get-$clientKey");
    $update_key = md5("notifications-update-$clientKey");
    $read_items = md5("items-read-items-$clientKey");
    $list_items = md5("items-list-items-$clientKey");
    
    $debug_out->SECTION_key = "notifications-$clientKey";
    $debug_out->SECTION_key = $section_key;
    
    //-------------------------------------------------------------------------
    // unpack message
    //-------------------------------------------------------------------------
    
    // generate section key from params
    $SECTION = generateKey("section", $section_key, $params);
    $debug_out->SECTION = $SECTION;
    
    // if the section key in data is what is expected
	if ($section == $SECTION) {
		
    	$debug_out->section_read = "NOTIFICATIONS";
    	
    	// add section to params collection to generate action
    	$params[] = $SECTION;
    	
    	// this service has four posible actions
	    $GET = generateKey("action", $get_key, $params);
	    $UPDATE = generateKey("action", $update_key, $params);
	    $READ_ITEMS = generateKey("action", $read_items, $params);
	    $LIST_ITEMS = generateKey("action", $list_items, $params);
	    
	    $debug_out->GET = $GET;
	    $debug_out->UPDATE = $UPDATE;
	    $debug_out->READ_ITEMS = $READ_ITEMS;	    
	    $debug_out->LIST_ITEMS = $LIST_ITEMS;
	    
	    // check if GET was requested
    	if ($action == $GET) {
    		if (!check_param($name_params, CS_PARAM_WHAT)) {
    			$isValid = false;
    			$response->error_code = CS_MISSING_PARAM;
    			
    			$debug_out->error_info = "Param '" . CS_PARAM_WHAT . "' was missing";
    		}
    		else {
    			$isValid = true;
    			$action_key = $get_key;
    		}    			    		

    		$debug_out->action_read = "GET";    		
    	}
    	// check if UPDATE was requested
    	else if ($action == $UPDATE) {
    		if (!check_param($name_params, CS_PARAM_UPDATE_COLLECTION)) {
    			$isValid = false;
    			$response->error_code = CS_MISSING_PARAM;
    			
    			$debug_out->error_info = "Param '" . CS_PARAM_UPDATE_COLLECTION . "' was missing";
    		}
    		else {
    			$isValid = true;
    			$action_key = $update_key;    		
    		}
    		
    		$debug_out->action_read = "UPDATE";    		
    	}
	    // check if READ_ITEMS was requested
    	else if ($action == $READ_ITEMS) {
   			$isValid = true;
   			$action_key = $read_items;
   			    		
    		$debug_out->action_read = "READ_ITEMS";    		
    	}
		// check if LIST_ITEMS was requested
    	else if ($action == $LIST_ITEMS) {
   			$isValid = true;
   			$action_key = $list_items;    	
   				
    		$debug_out->action_read = "LIST_ITEMS";    		
    	}
    	// the action is not valid
    	else {
    		$isValid = false;
    		$invalid_message = true;
    		$response->error_code = CS_INVALID_ACTION;
    		
    		$debug_out->action_read = "UNKNOW";
    	}
   		// finally check the signature
    	if ($isValid) {
    		// add actions to params collection to generate signature
    		$params[] = $action;
    		
    		$debug_out->signature_params = $nonce . $timestamp . $section_key . $action . $url . $login . $p;
    		
    		// generate signature from params
    		$SIGNATURE = generateKey("signature", $nonce . $timestamp . $section_key . $action_key . $url . $login . $p, $params);
    		
    		$debug_out->SIGNATURE = $SIGNATURE;
    		
    		// check if signature is valid
    		if ($signature == $SIGNATURE) {

    			$debug_out->is_signature_valid = true;    			
    		}
    		else {
    			$response->error_code = CS_INVALID_SIGNATURE;
    			
    			$debug_out->is_signature_valid = false;
    			$debug_out->signature_read = $signature;
    			$debug_out->signature_expected = $SIGNATURE;
    			$invalid_message = true;
    		}
    	}
	}
    else { 
		$response->error_code = CS_INVALID_SECTION;
		$invalid_message = true;
		
		$debug_out->section_read = "UNKNOW, section_expected : $SECTION";
    }

    if ($debug)
    	$response->debug = $debug_out;

    //-------------------------------------------------------------------------
    // process request
    //-------------------------------------------------------------------------
    
    if (!$invalid_message) {
    	
    	cs_db_connect($cs_server, $cs_user, $cs_password, $cs_database);
    	
    	$cu_id = cs_user_get_cu_id($login);
    	
    	if ($action == $GET) {
	    	try {
	    		$response->data = get_notifications($name_params[CS_PARAM_WHAT], $cu_id);
	    		$response->status = 1;
	    	} catch(Exception $e) {
	    		$response->error_code = CS_UNEXPECTED_ERROR;
	    		$response->status = 0;
	    	}
    	}
    	else if ($action == $UPDATE) {
    		try {
				$response->data = update_notifications($name_params[CS_PARAM_UPDATE_COLLECTION], $cu_id);
				$response->status = 1;
    		} catch(Exception $e) {
	    		$response->error_code = CS_UNEXPECTED_ERROR;
	    		$response->status = 0;
	    	}
    	}
       	else if ($action == $READ_ITEMS) {
    		try {
				$response->data = read_items($cu_id);
				$response->status = 1;
    		} catch(Exception $e) {
	    		$response->error_code = CS_UNEXPECTED_ERROR;
	    		$response->status = 0;
	    	}
    	}
        else if ($action == $LIST_ITEMS) {
    		try {
				$response->data = list_items($cu_id);
				$response->status = 1;
    		} catch(Exception $e) {
	    		$response->error_code = CS_UNEXPECTED_ERROR;
	    		$response->status = 0;
	    	}
    	}
    }
    else 
    	$response->status = 0;
    
    //-------------------------------------------------------------------------
    // response
    //-------------------------------------------------------------------------
    
    echo json_encode($response);

    //-------------------------------------------------------------------------
    // functions
    //-------------------------------------------------------------------------
    
    function generateKey($name, $key, $p) {
    	$a = $p;
    	sort($a);
    	
    	$joined = "";
    	foreach ($a as $e) {
    		$joined .= ($joined == "" ? "" : "&") . $e;
    	}
    	
    	global $debug_out;
    	$debug_out->joined[] = $joined;
    	
    	return $name . "=" . md5($joined . $key);
    }
    
    function check_param($params, $param_name) {
    	if (!isset($params))
    		return false;
    	if (!is_array($params))
    		return false;
    	return isset($params[$param_name]);    	 
    }
    
    function get_notifications($what, $cu_id) {
    	$sqlstmt = "select * from notification where nt_processed = 0 and trim(nt_message_info) <> '' and nt_topic = ? and sent < ? and cu_id = ? order by sent limit ?";
    	$qparams = array();
    	$qparams[] = array("s" => $what); 
    	$qparams[] = array("i" => MAX_SENT);
    	$qparams[] = array("i" => $cu_id);
    	$qparams[] = array("i" => NOTIFICATION_ROW_LIMIT);
		$result = cs_db_query($sqlstmt, $qparams);

		$rows = array();
	
		while ($row = $result->fetch_assoc()) {
			$rows[] = $row;
		}
		update_sent($rows, "notification", "nt_id");
		
		return $rows;
    }
    
    function update_sent($rows, $table, $id_column) {
    	$sqlstmt = "update $table set sent = sent + 1 where $id_column = ?";
    	
    	foreach($rows as $row) {
    		$qparams = array();
    		$qparams[] = array("i" => $row[$id_column]);
    		cs_db_query($sqlstmt, $qparams);
    	}
    }
    
    function update_notifications($json_update_collection, $cu_id) {
    	$rows_afected = 0;
    	$update_collection = json_decode($json_update_collection);
    	
    	$sqlstmt = "update notification set nt_processed = 1 where nt_id = ? and cu_id = ?";
    	
    	foreach($update_collection as $record) {
    		$qparams = array();
    		$qparams[] = array("i" => $record->nt_id); 
    		$qparams[] = array("i" => $cu_id);
    		cs_db_query($sqlstmt, $qparams);
    		$rows_afected++;
    	}
    	$result[] = array("rows_affected" => $rows_afected);
    	return $result;
    }
    
    function read_items($cu_id) {
    	cs_item_get_items($cu_id);
    	$rows = array();
    	return $rows;
    }
    
    function list_items($cu_id) {
    	$sqlstmt = "select * from item where sent = 0 and cu_id = ? limit ?";
    	$qparams = array();
    	$qparams[] = array("i" => $cu_id);
    	$qparams[] = array("i" => ITEMS_ROW_LIMIT);
    	$result = cs_db_query($sqlstmt, $qparams);
    	 
    	$rows = array();
    	 
    	while ($row = $result->fetch_assoc()) {
    		$rows[] = $row;
    	}
    	update_sent($rows, "item", "it_id");
    	 
    	return $rows;
    }
    
?>